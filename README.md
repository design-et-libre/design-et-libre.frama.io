# Sources du site https://design-et-libre.frama.io/

## Installation pour essais sur son propre PC

Cloner le dépôt sur son ordinateur

    git clone git@framagit.org:design-et-libre/design-et-libre.frama.io.git


Il faut ensuite installer un environnement virtuel et les librairies nécessaires

    virtualenv ve/
    source ve.bin/activate
    pip install -r requirements.txt


Et ensuite, vous pouvez modifier le thème *theme/blue-penguin* et la page
d'accueil *content/pages/home.md* où tout est stocké en Markdown.

Pour recalculer le site, il suffit de lancer pelican

    pelican

Vous pouvez maintenant ouvrir la page *index.html* qui se trouve dans le
répertoire *output* !



**Fait avec amour et avec pelican**